#include <seatest.h>
#include <Ball.h>

void test_move()
{
    Ball b [] = {Ball(N), Ball(NE), Ball(E), Ball(SE), Ball(S), Ball(SW), Ball(W), Ball(NW)};

    for(int i=0; i<=NW; i++){
        b[i].move();
    }

    // Test poruszania w kierunku N
    assert_int_equal(0,b[N].getX());
    assert_int_equal(1,b[N].getY());

    // Test poruszania w kierunku NE
    assert_int_equal(1,b[NE].getX());
    assert_int_equal(1,b[NE].getY());

    // Test poruszania w kierunku E
    assert_int_equal(1,b[E].getX());
    assert_int_equal(0,b[E].getY());

    // Test poruszania w kierunku SE
    assert_int_equal(1,b[SE].getX());
    assert_int_equal(-1,b[SE].getY());

    // Test poruszania w kierunku S
    assert_int_equal(0,b[S].getX());
    assert_int_equal(-1,b[S].getY());

    // Test poruszania w kierunku SW
    assert_int_equal(-1,b[SW].getX());
    assert_int_equal(-1,b[SW].getY());

    // Test poruszania w kierunku W
    assert_int_equal(-1,b[W].getX());
    assert_int_equal(0,b[W].getY());

    // Test poruszania w kierunku NW
    assert_int_equal(-1,b[NW].getX());
    assert_int_equal(1,b[NW].getY());
}

void test_fixture_ball( void )
{
	test_fixture_start();
	run_test(test_move);
	test_fixture_end();
}
