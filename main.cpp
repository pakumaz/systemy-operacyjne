#include <iostream>

#include <Board.h>

using namespace std;

int main()
{
    Board board(false);

    board.startGame();

    board.draw();
    //board.drawPositions();

    board.stopGame();

	return 0;
}
