#include "Board.h"

Board::Board(bool m) : drawMark(m)
{
}

Board::~Board(){

    for(int i=0; i<VSIZE; i++)
        for(int j=0; j<HSIZE; j++){
            pthread_mutex_destroy(&fieldMutex[i][j]);
        }
}

void Board::startGame(){

    playGame = true;

    Direction directions [] = {W, NW, N, NE, E};

    for(int i=0; i<VSIZE; i++){
        for(int j=0; j<HSIZE; j++){

            field[i][j] = false;
            fieldMutex[i][j] = PTHREAD_MUTEX_INITIALIZER;
        }
    }

    for(int i=0; i<BALLS; i++){

        b[i] = new Ball(this, i+1, HSIZE/2-2+i, 1, (Direction) (i%8), (i+1)*100000);
        pthread_create( b_t+i, NULL, &Ball::moveLoopHellper, b[i]);
    }
}

void Board::stopGame(){

    playGame = false;

    for(int i=0; i<BALLS; i++){

        pthread_cancel(b_t[i]);
        pthread_join(b_t[i], NULL);
        delete(b[i]);
    }
}

void Board::drawPositions(){

    int moves = MOVES;
    while(moves-->0){

        for(int i=0; i<BALLS; i++){

            b[i]->showPosition();
        }

        cout << endl;
        sleep(1);
    }
}

void Board::draw(){

    initscr();

    WINDOW *board = newwin(VSIZE+2, HSIZE+2, 0, 0);
    box(board,0,0);

    WINDOW *usedFields = newwin(VSIZE+2, HSIZE+2, 0, HSIZE + 2);
    box(usedFields,0,0);

    int moves = MOVES;
    while(moves--){

        // Czyszczenie okna
        if(drawMark == false){

            board = newwin(VSIZE+2, HSIZE+2, 0, 0);
            box(board,0,0);
        }

        for(int i=0; i<BALLS; i++){

            mvwaddch(board, VSIZE-b[i]->getY(), HSIZE-b[i]->getX(), b[i]->getNumber());
        }

        drawUsedFields(usedFields, moves);

        curs_set(0);

        wrefresh(board);
        usleep(50000);
    }

    endwin();
}

void Board::drawUsedFields(WINDOW *w, int moves){

    w = newwin(VSIZE+2, HSIZE+2, 0, HSIZE + 2);
    box(w,0,0);

    int usedFields = 0;

    for(int i=0; i<VSIZE; i++)
        for(int j=0; j<HSIZE; j++)
            if(isUsed(0,j,i)){
                usedFields ++;
                mvwaddch(w, VSIZE-i, HSIZE-j, '*');
            }

    for(int i=0; i<BALLS; i++){

        mvwprintw(w,i+2,1,"%c: (%2d,%2d):%d", b[i]->getNumber(),b[i]->getX(),b[i]->getY(),b[i]->getDirection());
    }

    mvwprintw(w, 1, 1, "%d:%d", moves, usedFields);

    wrefresh(w);
}
