#include "Ball.h"

void Ball::moveLoop(){

    while(board->isGameRun()){

        usleep(speed);
        //sleep(1);

        board->setUsed(number, getX(), getY(), false);

        move();

        if(board->isUsed(number, getX(), getY())){

            changeDirection();
            move();
        }

        board->setUsed(number, getX(), getY(), true);
    }
}

void Ball::move(){

    switch(direction){

        case (N):

            y++;
            if(getY() >= VSIZE-1) changeDirection(S);
            break;

        case (NE):

            x++;
            y++;
            if(getX() >= HSIZE-1) changeDirection(NW);
            if(getY() >= VSIZE-1) changeDirection(SE);
            break;

        case (E):

            x++;
            if(getX() >= HSIZE-1) changeDirection(W);
            break;

        case (SE):

            x++;
            y--;
            if(getX() >= HSIZE-1) changeDirection(SW);
            if(getY() <= 0) changeDirection(NE);
            break;

        case (S):

            y--;
            if(getY() <= 0) changeDirection(N);
            break;

        case (SW):

            x--;
            y--;
            if(getX() <= 0) changeDirection(SE);
            if(getY() <= 0) changeDirection(NW);
            break;

        case (W):

            x--;
            if(getX() <= 0) changeDirection(E);
            break;

        case (NW):

            x--;
            y++;
            if(getX() <= 0) changeDirection(NE);
            if(getY() >= VSIZE-1) changeDirection(SW);
            break;
    }
}

void Ball::showPosition(){

    cout << number << ": (" << getX() << ", " << getY() << ", " << direction << ")" << endl;
}

void Ball::changeDirection(Direction d){

    direction = d;
}

void Ball::changeDirection(){


    switch(direction){

        case (N):

            changeDirection(S);
            break;

        case (NE):

            changeDirection(SW);
            break;

        case (E):

            changeDirection(W);
            break;

        case (SE):

            changeDirection(NW);
            break;

        case (S):

            changeDirection(N);
            break;

        case (SW):

            changeDirection(NE);
            break;

        case (W):

            changeDirection(E);
            break;

        case (NW):

            changeDirection(SE);
            break;
    }
}
