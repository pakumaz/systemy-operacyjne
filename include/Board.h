#ifndef BOARD_H
#define BOARD_H

#define BALLS 20
#define MOVES 1000

#include <iostream>
#include <ncurses.h>
#include <pthread.h>
#include "Ball.h"
 using namespace std;

class Ball;

class Board
{
    public:

        Board(bool m);
        ~Board();

        void draw();
        void drawPositions();

        // Sprawdzenie czy pole jest zajęte
        bool isUsed(int nr, int x, int y){

            lockField(nr,x,y,"check");
            bool isUsedField = field[y][x];
            unlockField(nr,x,y,"check");

            return isUsedField;
        };

        // Ustawienie zajętości pola
        void setUsed(int nr, int x, int y, bool status){

            lockField(nr,x,y,"set");
            field[y][x] = status;
            unlockField(nr,x,y,"set");
        }

        bool isGameRun(){ return playGame; };
        void startGame();
        void stopGame();

    protected:
    private:

        bool playGame;

        Ball *b [BALLS];
        pthread_t b_t [BALLS];

        // Rysowanie śladu piłek
        bool drawMark;

        // Zajętość pola
        bool field[VSIZE][HSIZE];
        pthread_mutex_t fieldMutex[VSIZE][HSIZE];

        void lockField(int nr, int x, int y, char *action){ pthread_mutex_lock(&fieldMutex[y][x]); };
        void unlockField(int nr, int x, int y, char *action){ pthread_mutex_unlock(&fieldMutex[y][x]); };

        void drawUsedFields(WINDOW *w, int moves);
};

#endif // BOARD_H
