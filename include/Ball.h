#ifndef BALL_H
#define BALL_H

// Big board
#define HSIZE 78
#define VSIZE 22

// Smal board
//#define HSIZE 8
//#define VSIZE 8

#include <iostream>
#include <pthread.h>
#include "Board.h"
 using namespace std;

/**
 * Kierunki porszuania się piłki:
 *
 * 0 - N  - północ
 * 1 - NE - pólnocny wschód
 * 2 - E  - wschód
 * 3 - SE - połódniowy wschód
 * 4 - S  - południe
 * 5 - SW - południowy zachód
 * 6 - w  - zachód
 * 7 - NW - północny zachód
 */
enum Direction { N, NE, E, SE, S, SW, W, NW };

class Board;

class Ball
{
    public:

        Ball( Board *b, int n = 0, int x = 0, int y = 0, Direction d = N, int s = 1) : board(b), number(n), x(x), y(y), direction(d), speed(s) {};

        // Zmiana pozycji piłki
        static void *moveLoopHellper(void *b)
        {
            ((Ball *)b)->moveLoop();
        }
        void moveLoop();
        void move();

        // Wyświetlenie pozycji i kierunku lotu
        void showPosition();

        // Pobranie znaku piłki
        char getNumber(){ return (int) '0' + number; };

        // Pobranie pozycji piłki
        int getX(){ return x; };
        int getY(){ return y; };

        // Pobranie kierunku ruchu
        int getDirection(){ return direction; };

    protected:
    private:

        // Komora piłki
        Board *board;

        // Numer kulki
        int number;

        // Współrzędne położenia
        int x;
        int y;

        // Kierunek poruszania się piłki
        Direction direction;

        // Prędkość poruszania się
        int speed;

        // Zmiana kierunku poruszania się
        void changeDirection(Direction d);
        void changeDirection();
};

#endif // BALL_H
